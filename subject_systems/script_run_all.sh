if [ -z "$1" ]
	then
	echo "appName file is missing is missing (#1)"
	exit 0
fi
if [ -z "$2" ]
	then
	echo "analName file is missing is missing is missing (#2)"
	exit 0
fi


appNameFile="$1"
analNameFile="$2"
outputFolder="results"

#remove all obj and xlsx files in the current directory
for extension in {obj,xlsx};do
	for file in "*.$extension"; do
		rm $file
	done
done

#remove everything (obj and xlsx) in the result directory
rm $outputFolder/*


while read app;do
	while read anal; do
		echo $app $anal
		#for every application and analyses - the log file is _.log usually, the resutl will be 
		./script_SmellExcell.sh $app/$anal/*.log $outputFolder/
	done <$analNameFile
done <$appNameFile

for extension in {obj,xlsx};do
	for file in "*.$extension"; do
		mv $file $outputFolder/
	done

done

./script_StatisticallAnalysis.sh $outputFolder

exit
