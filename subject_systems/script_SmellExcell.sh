
if [ -z "$1" ]
	then
	echo "Input file is missing"
	exit 0 ;
fi

if [ -z "$2" ]
	then
	echo "Output folder is missing"
	exit 0 ;
fi

inputFile="$1"
outputFolder="$2"

 /Library/Java/JavaVirtualMachines/jdk1.7.0_45.jdk/Contents/Home/bin/java -Dfile.encoding=MacRoman -classpath /Users/pooyan/workspaces/USC/Pooyan_Analyzer/bin:/Users/pooyan/workspaces/USC/Pooyan_Analyzer/lib/dom4j-1.6.1.jar:/Users/pooyan/workspaces/USC/Pooyan_Analyzer/lib/poi-3.9-20121203.jar:/Users/pooyan/workspaces/USC/Pooyan_Analyzer/lib/poi-ooxml-3.9-20121203.jar:/Users/pooyan/workspaces/USC/Pooyan_Analyzer/lib/poi-ooxml-schemas-3.9-20121203.jar:/Users/pooyan/workspaces/USC/Pooyan_Analyzer/lib/xmlbeans-2.3.0.jar JoshSEAParser "$inputFile"

for extension in {obj,xlsx};do
	for file in "*.$extension"; do
		mv $file $outputFolder/
	done

done