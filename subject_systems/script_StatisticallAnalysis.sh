
if [ -z "$1" ]
	then
	echo "Result (containing obj and xlsx) folder is missing"
	exit 0 ;
fi

inputFolder="$1"
/Library/Java/JavaVirtualMachines/jdk1.7.0_45.jdk/Contents/Home/bin/java -Dfile.encoding=US-ASCII -classpath /Users/pooyan/workspaces/USC/Pooyan_Analyzer/bin:/Users/pooyan/workspaces/USC/Pooyan_Analyzer/lib/dom4j-1.6.1.jar:/Users/pooyan/workspaces/USC/Pooyan_Analyzer/lib/poi-3.9-20121203.jar:/Users/pooyan/workspaces/USC/Pooyan_Analyzer/lib/poi-ooxml-3.9-20121203.jar:/Users/pooyan/workspaces/USC/Pooyan_Analyzer/lib/poi-ooxml-schemas-3.9-20121203.jar:/Users/pooyan/workspaces/USC/Pooyan_Analyzer/lib/xmlbeans-2.3.0.jar:/Users/pooyan/workspaces/USC/Pooyan_Analyzer/stdlib.jar StatisticalAnalysis $inputFolder