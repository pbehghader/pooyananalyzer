if [ -z $1 ] 
	then
	echo "Application's name is missing"
	exit 0 ;
fi
if [ -z $2 ]
	then
	echo "Input file is missing"
	exit 0 ;
fi
appName=$1
inputFiel=$2

 /Library/Java/JavaVirtualMachines/jdk1.7.0_45.jdk/Contents/Home/bin/java -Dfile.encoding=MacRoman -classpath /Users/pooyan/workspaces/USC/Josh-SEA-Parser/bin:/Users/pooyan/workspaces/USC/Josh-SEA-Parser/lib/dom4j-1.6.1.jar:/Users/pooyan/workspaces/USC/Josh-SEA-Parser/lib/poi-3.9-20121203.jar:/Users/pooyan/workspaces/USC/Josh-SEA-Parser/lib/poi-ooxml-3.9-20121203.jar:/Users/pooyan/workspaces/USC/Josh-SEA-Parser/lib/poi-ooxml-schemas-3.9-20121203.jar:/Users/pooyan/workspaces/USC/Josh-SEA-Parser/lib/xmlbeans-2.3.0.jar JoshSEAParser $appName $inputFiel