import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TextTool {
	public static void main(String args[]) {
	}

	public static String formatCorrector(String text) {
		Pattern[] patterns = new Pattern[2];
		patterns[0] = Pattern.compile("[0-9]+\\.");
		patterns[1] = Pattern.compile("[0-9]+");
		StringBuilder strBuilder = new StringBuilder("");
		int start = 0;
		int end = 0;
		int patternNum = 0;
		int numOfMatchs = 0;
		int totalEnd = 0;
		for (Pattern pattern : patterns) {
			Matcher matcher = pattern.matcher(text.substring(totalEnd,
					text.length()));
			while (matcher.find()) {
				numOfMatchs++;
				start = matcher.start();
				if (start != end & totalEnd != 0 & patternNum == 0)
					break;
				end = matcher.end();
				String ta = matcher.group();
				while (ta.length() < 4 - patternNum & patternNum < 2)
					ta = "0" + ta;
				strBuilder.append(ta);
				if (patternNum == 1)
					break;
			}

			if (patternNum == 1)
				while (numOfMatchs < 3) {
					strBuilder.append(".000");
					numOfMatchs++;
				}
			totalEnd += end;
			patternNum++;
		}
		String left = text.substring(totalEnd, text.length());
		if (left.charAt(0) != '-')
			strBuilder.append("-a-");
		strBuilder.append(left);
		String ret = strBuilder.toString();
		ret = ret.toLowerCase().replaceAll("alpha", "b-apha")
				.replaceAll("beta", "c-beta").replaceAll("final", "z-final");
		return ret;
	}

	public static String findFirstMatchIssue(String text, String regEx) {
		Pattern pattern = Pattern.compile(regEx);
		Matcher matcher = pattern.matcher(text);
		while (matcher.find())
			return matcher.group();
		return "";

	}

}
