import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;


public class DSReaderWriter{

	/**
	 * @param args
	 */
	
	public static void main (String[] args){
		Map<String,HashMap<String,Integer>> m = new HashMap<String,HashMap<String,Integer>> () ;
		m.put("hey", null);
		
		writeF2S ("t1",new File2Smell(m));
		File2Smell f2s = readF2S("t1");
		m = f2s.content;
		System.out.println(m);
		System.out.println(f2s.content);
	}
	public static void writeF2S (String fileName,File2Smell f2s) {
		write (fileName,f2s);
	}
	public static File2Smell readF2S (String fileName){
		return (File2Smell) read (fileName);
	}
	
	private static void write (String fileName,Object o) {
		// TODO Auto-generated method stub
		FileOutputStream fileOut;
		try {
			fileOut = new FileOutputStream(fileName+".obj");
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(o);
			out.close();
			fileOut.close();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	private static Object read (String fileName){
		FileInputStream fileIn;
		Object o=null;
		try {
			fileIn = new FileInputStream(fileName+".obj");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			o = in.readObject();
			in.close();
			fileIn.close();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return o;
	}

}
