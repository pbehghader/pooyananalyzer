import java.io.File;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JoshSEAParser {

	/**
	 * @param args
	 */

	static PrintStream err = System.out;
	static File2Smell file2Smell = new File2Smell();

	public static void main(String[] args) throws Exception {
		// The input file should be log file STIC analyses (Smell to Issue
		// Correlation)
		String inputFile = args[0];
		String outputFileName = (inputFile.substring(0,
				inputFile.lastIndexOf("."))).replaceAll("/", "")
				.replaceAll("\\.", "").replaceAll("_", "");
		Scanner sc = new Scanner(new File(inputFile));
		int state = 0; // start
		int nextState = 0;
		String fileName = "";
		while (sc.hasNext()) {
			String line = sc.nextLine();
			if (line.contains("DEBUG") == false)
				continue;
			switch (state) {
			case 0: // start
				//System.out.println(0);
				if (line.contains(".ser")
						&& line.lastIndexOf(".ser") == line.length() - 4) {
					nextState = 1; // new file
					fileName = filePreparation(line);
				} else
					throw (new Exception("incorrect format: " + line + " -> "
							+ line.lastIndexOf(".ser") + 4 + " != "
							+ line.length() + " " + line.contains(".ser")));
				break;
			case 1: // read total smells
				//System.out.println("1");
				int totalSmells = readTotalSmells(fileName, line);
				if (totalSmells == 0)
					nextState = 4; // read the listing line, althoug there is no
									// smell
				else
					nextState = 2;
				break;
			case 2: // this line: "Listing detected smells for *.ser"
				//System.out.println(2);
				nextState = 3;
				break;
			case 3: // read smells or new File
				//System.out.println(3);
				if (line.contains(".ser")
						&& line.lastIndexOf(".ser") == line.length() - 4) {
					nextState = 1; // newfile
					fileName = filePreparation(line);
				} else {
					readSmell(fileName, line);
					nextState = 3;
				}
				break;
			case 4: // this line: "Listing detected smells for *.ser" then go
					// for a new file
				//System.out.println(4);
				nextState = 0;
				break;
			}
			state = nextState;
		}
		WriteExcelDemo.writeSmells(outputFileName, file2Smell);
		DSReaderWriter.writeF2S(outputFileName, file2Smell);
		File2Smell temp = DSReaderWriter.readF2S(outputFileName);
		err.println(temp.content);
	}

	private static void readSmell(String fileName, String line) {
		String[] keys = { "bco", "buo", "spf", "bdc" };
		for (int i = 0; i < keys.length; i++)
			if (line.contains(keys[i]+" ")) { //the are necassry since it may cause problems when the name of the file contains a smell name
				int currentValue = file2Smell.content.get(fileName)
						.get(keys[i]);
				file2Smell.content.get(fileName).put(keys[i], currentValue + 1);
				break;
			}
	}

	private static int readTotalSmells(String fileName, String line) {
		file2Smell.content.get(fileName).put("all",
				Integer.parseInt(line.replaceAll("[\\D]", "")));
		return file2Smell.content.get(fileName).get("all");
	}

	private static String filePreparation(String line) {
		String temp = line.substring(0, line.lastIndexOf("."));
		String temp2 = temp.substring(line.lastIndexOf(" ") + 1);
		// String fileName = temp2.substring(findMatch(temp2, "[0-9]"));
		// String fileName = formatCorrector(temp2);
		// String fileName = temp2 ;
		String fileName = TextTool.formatCorrector(temp2);
		file2Smell.content.put(fileName, new HashMap<String, Integer>());
		String[] keys = { "all", "bco", "buo", "spf", "bdc" };
		for (int i = 0; i < keys.length; i++)
			file2Smell.content.get(fileName).put(keys[i], 0);
		return fileName;
	}

	public static int findMatch(String text, String regex) {
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(text);
		// Check all occurrences
		while (matcher.find())
			return matcher.start();
		return 0;
	}

	public static String formatCorrector(String text) {
		Pattern pattern = Pattern.compile("[1-9]+");
		Matcher matcher = pattern.matcher(text);
		int start = 0;
		int end = 0;
		StringBuilder strBuilder = new StringBuilder("");
		while (matcher.find()) {
			start = matcher.start();
			end = matcher.end();
			String ta = matcher.group();
			while (ta.length() < 2)
				ta = "0" + ta;
			strBuilder.append(ta).append(".");
		}
		strBuilder.append(text.substring(end));
		return strBuilder.toString();
	}
}
