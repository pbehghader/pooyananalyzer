import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeSet;

;
public class StatisticalAnalysis {
	private static String FILE_DIR = null;
	private static final String FILE_TEXT_EXT = ".obj";

	public static void main(String args[]) {

		FILE_DIR = args[0]; // File contaning all obj and xlsx files
		String[] list = listFile(FILE_DIR, FILE_TEXT_EXT);
		String[] analyses = Constants.analyses;
		String[] smells = Constants.smells;
		String[] metrics = Constants.metrics;
		Map<String, HashMap<String, File2Smell>> infos = new HashMap<String, HashMap<String, File2Smell>>();
		for (String analysis : analyses)
			infos.put(analysis, new HashMap<String, File2Smell>());
		for (String fileNameE : list) {
			System.out.println(fileNameE);
			Map<String, File2Smell> info = null;
			for (String anal:analyses) // I should be care ful since some appcliation may contain the name of analyses in them
				if (fileNameE.contains(anal))
					info = infos.get(anal);
				
			String name = fileNameE.substring(0, fileNameE.lastIndexOf("."));
			File2Smell f2s = DSReaderWriter.readF2S(FILE_DIR + "/" + name);
			info.put(name, f2s);
		}
		Map<String, Map<String, Map<String, Map<String, Float>>>> data = new HashMap<String, Map<String, Map<String, Map<String, Float>>>>();
		for (String analysis : analyses) {
			data.put(analysis,
					new HashMap<String, Map<String, Map<String, Float>>>());
			Map<String, File2Smell> info = infos.get(analysis);
			for (String app : info.keySet()) {
				String appPure = app;
				for (String anal:analyses)
					appPure=appPure.replaceAll(anal, "");
					
//				String appPure = app.replaceAll("acdc", "").replaceAll("arc","");
				System.out.println(appPure);
				data.get(analysis).put(appPure,
						new HashMap<String, Map<String, Float>>());
				File2Smell f2s = info.get(app);
				Map<String, HashMap<String, Integer>> map = f2s.content;
				for (String smell : smells) {
					data.get(analysis).get(appPure)
							.put(smell, new HashMap<String, Float>());
					TreeSet<Integer> set = new TreeSet<Integer>();
					int numOfVersions = 0;
					int totalNumber = 0;
					int max = -1;
					int min = 10000;
					for (String ver : map.keySet()) {
						numOfVersions++;
						int v = map.get(ver).get(smell);
						totalNumber += v;
						if (v > max)
							max = v;
						if (v < min)
							min = v;
						set.add(v);
					}
					
					data.get(analysis).get(appPure).get(smell)
							.put(metrics[0], new Float(min));
					data.get(analysis).get(appPure).get(smell)
							.put(metrics[1], new Float(max));
					Float mean = new Float(-1.0);
					if (numOfVersions > 0)
						mean = ((float) totalNumber) / numOfVersions;

					data.get(analysis).get(appPure).get(smell)
							.put(metrics[2], mean);
					int[] array = new int[set.size()];
					{
						int i = 0;
						for (Integer s : set)
							array[i++] = s;
					}
					Float stddev=(float) StdStats.stddev(array);
					data.get(analysis).get(appPure).get(smell)
					.put(metrics[3], stddev);
				}
			}
		}
		WriteExcelDemo.writeStatistics("minmaxmeanstddev", data);
	}

	public static String[] listFile(String folder, String ext) {

		GenericExtFilter filter = new GenericExtFilter(ext);

		File dir = new File(folder);

		if (dir.isDirectory() == false) {
			System.out.println("Directory does not exists : " + FILE_DIR);
			return null;
		}

		// list out all the file name and filter by the extension
		String[] list = dir.list(filter);

		if (list.length == 0) {
			System.out.println("no files end with : " + ext);
			return null;
		}

		return list;
	}

	// inner class, generic extension filter
	public static class GenericExtFilter implements FilenameFilter {

		private String ext;

		public GenericExtFilter(String ext) {
			this.ext = ext;
		}

		public boolean accept(File dir, String name) {
			return (name.endsWith(ext));
		}
	}
}
