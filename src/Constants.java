
public class Constants {
	public static String[] analyses = { "arc", "acdc" , "pkg" };
	public static String[] smells = { "bco", "buo", "spf", "bdc" };
	public static String[] metrics = { "min", "max", "mean","stddev" };
}
