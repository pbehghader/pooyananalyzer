import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.Vector;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class WriteExcelDemo {

	public static void write(String outputFileName, Map<String, Object[]> data) {
		// Blank workbook
		XSSFWorkbook workbook = new XSSFWorkbook();

		// Create a blank sheet
		XSSFSheet sheet = workbook.createSheet("Employee Data");

		// Iterate over data and write to sheet
		Set<String> keyset = data.keySet();
		int rownum = 0;
		for (String key : keyset) {
			Row row = sheet.createRow(rownum++);
			Object[] objArr = data.get(key);
			int cellnum = 0;
			for (Object obj : objArr) {
				Cell cell = row.createCell(cellnum++);
				if (obj instanceof String)
					cell.setCellValue((String) obj);
				else if (obj instanceof Integer)
					cell.setCellValue((Integer) obj);
			}
		}
		try {
			// Write the workbook in file system
			String xlsx = outputFileName + ".xlsx";
			FileOutputStream out = new FileOutputStream(new File(xlsx));
			workbook.write(out);
			out.close();

			System.out.println(xlsx + " written successfully on disk.");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void writeStatistics(String outputfileName,
			Map<String, Map<String, Map<String, Map<String, Float>>>> info) {
		// Map<String, HashMap<String, Integer>> map = f2s.content;
		// SortedSet<String> keys = new TreeSet<String>(map.keySet());
		// This data needs to be written (Object[])
		Map<String, Object[]> data = new TreeMap<String, Object[]>();
		// String[] smells = { "all", "bco", "buo", "spf", "bdc" };
		// data.put("0",new
		// Object[]{"version",smells[1],smells[2],smells[3],smells[4],smells[0]});
		String[] analyses = Constants.analyses;
		Vector<String> analysisV = new Vector<String>();
		analysisV.add("version");
		String[] smells = Constants.smells;
		Vector<String> smellsV = new Vector<String>();
		smellsV.add("");
		String[] metrics = Constants.metrics;
		Vector<String> metricsV = new Vector<String>();
		metricsV.add("");// first columen
		for (String analysis : analyses)
			for (String smell : smells)
				for (String metric : metrics) {
					analysisV.add(analysis);
					smellsV.add(smell);
					metricsV.add(metric);
				}
		Object[] tempAnalyses = analysisV.toArray(new String[analysisV.size()]);
		data.put("0", tempAnalyses);
		Object[] tempSmells = smellsV.toArray(new String[smellsV.size()]);
		data.put("1", tempSmells);
		Object[] tempMetrics = metricsV.toArray(new String[metricsV.size()]);
		data.put("2", tempMetrics);
		for (String app : info.get(analyses[0]).keySet()) {
			Vector<String> values = new Vector<String>();
			values.add(app);
			for (String analysis : analyses) {
				for (String smell : smells) {
					for (String metric : metrics) {
						// System.out.println(app+":"+analysis+":"+smell+":"+metric+":");
						Float v = info.get(analysis).get(app).get(smell)
								.get(metric);
						values.add(v.toString());
						// System.out.println(v);

					}
				}
			}
			System.out.println(values);
			Object[] tempValues = values.toArray(new String[values.size()]);
			data.put(app, tempValues);
		}
		write(outputfileName, data);

	}

	public static void writeSmells(String outputfileName, File2Smell f2s) {
		Map<String, HashMap<String, Integer>> map = f2s.content;
		SortedSet<String> keys = new TreeSet<String>(map.keySet());
		// This data needs to be written (Object[])
		Map<String, Object[]> data = new TreeMap<String, Object[]>();
		String[] smells = { "all", "bco", "buo", "spf", "bdc" };
		data.put("0", new Object[] { "version", smells[1], smells[2],
				smells[3], smells[4], smells[0] });
		for (String key : keys) {
			Integer[] values = new Integer[5];
			for (int i = 0; i < smells.length; i++)
				values[i] = map.get(key).get(smells[i]);

			data.put(key, new Object[] { key, values[1], values[2], values[3],
					values[4], values[0] });
		}
		write(outputfileName, data);

	}
}
