import java.util.Scanner;
import java.util.Vector;

public class SysEvoParser {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		Double sum = new Double(0);
		Double sourceCoverage = new Double(0);
		Double targetCoverage = new Double(0);
		int n = 0;
		int srcCN = 0 ;
		int trgCN = 0 ;
		int c2cOrbse=-1;
		while (sc.hasNext()) {
			String line = sc.nextLine();
			if (line.contains("SysEvo") == true)
				c2cOrbse=0;
			else if ( line.contains("source coverage of"))
				c2cOrbse=1;	
			else if ( line.contains("target coverage of"))
				c2cOrbse=2;
			else continue;
			
			String numStr = line.substring(line.lastIndexOf(":") + 2);
			System.out.println(c2cOrbse + " : " + numStr);
			
			
			Double numberDouble = Double.parseDouble(numStr);
			if (c2cOrbse==0){
				sum += numberDouble;
				n++;
			}
			else if (c2cOrbse==1){
				sourceCoverage+=numberDouble;
				srcCN++;
			}
			else if (c2cOrbse==2){
				targetCoverage+=numberDouble;
				trgCN++;
			}	
		}
		if (n>0){
			Double avg = sum / n;
			System.out.println("For BSE num : " + n);
			System.out.println("For BSE avg : " + avg);
		}
		if (srcCN>0){
			Double avg = sourceCoverage / srcCN;
			System.out.println("For C2C -source coverage to- num : " + srcCN);
			System.out.println("For C2C -source coverage to- avg : " + avg);
		}
		if (trgCN>0){
			Double avg = targetCoverage / trgCN;
			System.out.println("For C2C -target coverage to- num : " + trgCN);
			System.out.println("For C2C -target coverage to- avg : " + avg);
			
		}

	}

}
