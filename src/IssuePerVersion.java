import java.util.Scanner;

public class IssuePerVersion {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int sum = 0 ;
		while (sc.hasNext()){
			String r = TextTool.findFirstMatchIssue(sc.nextLine(), "=[0-9]+");
			sum+= Integer.parseInt(r.substring(1));
		}
		System.out.println(sum);
	}

}
