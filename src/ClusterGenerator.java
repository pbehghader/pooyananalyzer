import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Random;


public class ClusterGenerator {

	/**
	 * @param args
	 * @throws UnsupportedEncodingException 
	 * @throws FileNotFoundException 
	 */
	public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException {
		// TODO Auto-generated method stub
		for ( int i=0; i<100; i++) {
			PrintWriter writer = new PrintWriter("pooyan_c"+i+".rsf", "UTF-8");
			for (int j=1;j<100;j++){
				int r=randInt(-15,20);
				if (r<=0)
					continue;
				writer.println("contain "+r+" "+j);
			}
			writer.close();
		}
	}
	public static int randInt(int min, int max) {
	    Random rand = new Random();
	    int randomNum = rand.nextInt((max - min) + 1) + min;
	    return randomNum;
	}
}
