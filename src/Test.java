import java.text.DecimalFormat;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Test {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		while (in.hasNext()) {
			String str = in.nextLine();
			// System.out.println(str.substring(findMatch(str, "[0-9]+")));
			System.out.println();
			System.out.println(str);
			System.out.println(TextTool.formatCorrector(str));
		}
	}

	public static int findMatch(String text, String regex) {
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(text);
		// Check all occurrences
		while (matcher.find())
			return matcher.start();
		return 0;
	}

}
